from flask import Flask, render_template, request
from flask_mysqldb import MySQL
app = Flask(__name__)

app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'covid19'

mysql = MySQL(app)

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == "POST":
        details = request.form
        username = details['username']
        password = details['password']
        if username and password:
            cur = mysql.connection.cursor()
            login_query = "SELECT password FROM users WHERE username='%s';" % (username)
            q = cur.execute(login_query)
            results = cur.fetchall()
            cur.close()
            if q == 1: # NO ERROR, USER INTO THE TABLE
                password_in_db = results[0][0]
                if password == password_in_db:
                    print("logged!")
                    add_patient()
                    return render_template('add_patient.html')
                else:
                    print("not logged!")
    return render_template('index.html')

@app.route('/add_patient', methods=['GET', 'POST'])
def add_patient():
    if "add_patient" not in request.url:
        return render_template('add_patient.html')

    table_columns = ("medico", "data_ora", "cognome", "nome", "data_nascita", "luogo_nascita", "indirizzo", "comune", "provincia", "tel_fisso", "tel_cellulare",
                     "dipentente_asl", "numero_conviventi", "interlocutore", "soggiorno_a_rischio", "paese_a_rischio", "data_partenza_zona_rischio", "contatti_casi_accertati",
                     "contatti_casi_rischio", "presenti_sintomi", "data_comparsa_sintomi", "tosse", "mal_di_gola", "dolori_muscolari", "malessere_generale", "vomito",
                     "respiro_accelerato", "difficolta_respirare_lieve_sforzo", "temperatura_superiore38", "temperatura", "vaccinazione_antinfluenzale", "assunzione_farmaci",
                     "elenco_farmaci", "note", "paziente_sottoposto_a", "deceduto", "guarito")

    mysql_columns = "(id, "
    mysql_values = "(NULL, "

    print("Add patient")
    if request.method == "POST":
        details = request.form

        for k in table_columns:

            # mysql_columns
            if k != "guarito":
                mysql_columns += "%s, " % (k)
            else:
                mysql_columns += "%s)" % (k)

            # mysql_values
            if k in ["numero_conviventi", "temperatura"]: # int, float
                mysql_values += str(details[k]) + ", "
            elif k in ["dipentente_asl", "contatti_casi_accertati", "contatti_casi_rischio", "presenti_sintomi", "tosse", "mal_di_gola", "dolori_muscolari",
                       "malessere_generale", "vomito", "respiro_accelerato", "difficolta_respirare_lieve_sforzo", "temperatura_superiore38", "vaccinazione_antinfluenzale",
                       "assunzione_farmaci", "deceduto", "guarito"]: # boolean
                if k in details.to_dict().keys() and k != "guarito":
                    mysql_values += "true, "
                elif k in details.to_dict().keys() and k == "guarito":
                    mysql_values += "true)"
                elif k not in details.to_dict().keys() and k != "guarito":
                    mysql_values += "false, "
                elif k not in details.to_dict().keys() and k == "guarito":
                    mysql_values += "false)"
            elif k in ["data_ora", "data_nascita", "data_partenza_zona_rischio", "data_comparsa_sintomi"]: #datetime
                print(details[k])
                if details[k] == "": #EMPTY
                    mysql_values += "'0000-00-00T00:00:00', "
                elif "T" in details[k]: # DATETIME
                    mysql_values += "'" + str(details[k]) + ":00', "
                elif "T" not in details[k]: # DATE
                    mysql_values += "'" + str(details[k]) + "T00:00:00', "
            else: # string
                if details[k] == "":
                    mysql_values += "'', "
                else:
                    mysql_values += "'" + str(details[k]) + "', "

        mysql_command = "INSERT INTO 'anagrafica' %s VALUES %s;" % (mysql_columns, mysql_values)
        print(mysql_command)
        cur = mysql.connection.cursor()
        #try:
        cur.execute(mysql_command)
        mysql.connection.commit()
        #except:
        #    print("ERROR")
        print(mysql_command)

    return render_template('add_patient.html')

@app.route('/query', methods=['GET', 'POST'])
def query():
    print("query")
    return render_template('query.html')

if __name__ == '__main__':
    app.run()

